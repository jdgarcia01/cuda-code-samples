/*
 ============================================================================
 Name        : tut1.cu
 Author      : John Garcia
 Version     :
 Copyright   : Your copyright notice
 Description : CUDA compute reciprocals
 ============================================================================
 */

#include <iostream>
#include <numeric>
#include <stdlib.h>
#include <math.h>
#include  "../../cuda_by_example/common/book.h"
// Headers that I will use later on with
// other examples.
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <OpenGL/glext.h>
#include <GLUT/glut.h>


using namespace std;

/**
 * Keep N <= 65535.
 * As we are not handling this limitation.
 */
#define N 500


/**
 * This function will use the cuda GPU to
 * add up some matrixs.
 */
__global__ void kernel(int *a, int *b, int *c){

	int tid = blockIdx.x;

	if(tid < N)
		c[tid] = a[tid] + b[tid];

}

//
// Fill the array on the GPU.
//
__global__ void kernel_fill_array(int *a, int *b){

	int tid = blockIdx.x;

	if( tid < N ){

		a[tid] = tid + 2;
		b[tid] = tid + 10;

	}

}

int main(void){


	int a[N], b[N], c[N];
	int *dev_a, *dev_b, *dev_c;


	cudaMalloc((void**)&dev_a, N * sizeof(int));
	cudaMalloc((void**)&dev_b, N * sizeof(int));
	cudaMalloc((void**)&dev_c, N * sizeof(int));


	cudaMemcpy(dev_a, a, N * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(dev_b, b, N * sizeof(int), cudaMemcpyHostToDevice);

	 cout << "Filling the array on the GPU!" << endl;

	kernel_fill_array<<<N,1>>>(dev_a,dev_b);

	//execute the function on the GPU.
	// execute using size BLOCKS.
	cout << "Calling addition on the GPU" << endl;
	kernel<<<N,1>>>(dev_a, dev_b, dev_c);



	cudaMemcpy(c, dev_c, N * sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(a, dev_a, N * sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(b, dev_b, N * sizeof(int), cudaMemcpyDeviceToHost);

	cout << "display the solutions!" << endl;
	for(int i = 0; i < N; i++){
		printf("%d + %d = %d\n", a[i], b[i], c[i]);
	}

	cudaFree(dev_a);
	cudaFree(dev_b);
	cudaFree(dev_c);

	return 0;


	
}
